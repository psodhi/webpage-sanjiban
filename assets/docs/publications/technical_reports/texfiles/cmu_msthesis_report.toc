\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Approach}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Contributions}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Organization}{1}{section.1.3}
\contentsline {chapter}{\numberline {2}Background}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Related Work in Robotic High-Throughput Phenotyping Platforms}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Related Work in Plant Imaging and Phenotype Estimation}{3}{section.2.2}
\contentsline {chapter}{\numberline {3}Multi-view 3D Reconstruction of Sorghum​ Plants}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}3D Reconstruction in Indoor Environments}{5}{section.3.1}
\contentsline {section}{\numberline {3.2}3D Reconstruction in Field Environments}{5}{section.3.2}
\contentsline {chapter}{\numberline {4}Segmentation and Identification of Plant Structures}{7}{chapter.4}
\contentsline {section}{\numberline {4.1}Semantic Segmentation of 3D Point Clouds of Plants}{7}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Local feature extraction}{7}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Global feature extraction}{7}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Support Vector Machine (SVM) Classifier}{7}{subsection.4.1.3}
\contentsline {section}{\numberline {4.2}Spatial Smoothing of 3D Point Clouds of Plants}{7}{section.4.2}
\contentsline {section}{\numberline {4.3}Extraction of Plant Phytomers}{7}{section.4.3}
\contentsline {chapter}{\numberline {5}Plant Phenotyping}{9}{chapter.5}
\contentsline {section}{\numberline {5.1}Phenotyping using Computational Geometry methods}{9}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Stem Diameter}{9}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Leaf Area}{9}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Leaf Length and Width}{9}{subsection.5.1.3}
\contentsline {section}{\numberline {5.2}Phenotyping using Generative Models}{9}{section.5.2}
\contentsline {chapter}{\numberline {6}Conclusions}{11}{chapter.6}
\contentsline {section}{\numberline {6.1}Contributions}{11}{section.6.1}
\contentsline {section}{\numberline {6.2}Future Work}{11}{section.6.2}
\contentsline {chapter}{Bibliography}{13}{chapter*.2}
