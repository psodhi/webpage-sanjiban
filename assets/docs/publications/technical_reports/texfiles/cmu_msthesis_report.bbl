\begin{thebibliography}{10}

\bibitem{li2014review}
Lei Li, Qin Zhang, and Danfeng Huang.
\newblock A review of imaging techniques for plant phenotyping.
\newblock {\em Sensors}, 14(11):20078--20111, 2014.

\bibitem{riberaestimating}
Javier Ribera, Fangning He, Yuhao Chen, Ayman~F Habib, and Edward~J Delp.
\newblock Estimating phenotypic traits from uav based rgb imagery.

\bibitem{bangert2013field}
Waldemar Bangert, Arnd Kielhorn, Florian Rahe, Amos Albert, Peter Biber,
  Slawomir Grzonka, Sebastian Haug, Andreas Michaels, Daniel Mentrup, Martin
  H{\"a}nsel, et~al.
\newblock Field-robot-based agriculture:“remotefarming. 1” and
  “bonirob-apps”.
\newblock {\em VDI-Berichte}, (2193):439--446, 2013.

\bibitem{virlet2017field}
Nicolas Virlet, Kasra Sabermanesh, Pouria Sadeghi-Tehran, and Malcolm~J
  Hawkesford.
\newblock Field scanalyzer: An automated robotic field phenotyping platform for
  detailed crop monitoring.
\newblock {\em Functional Plant Biology}, 44(1):143--153, 2017.

\bibitem{weiss2011plant}
Ulrich Weiss and Peter Biber.
\newblock Plant detection and mapping for agricultural robots using a 3d lidar
  sensor.
\newblock {\em Robotics and autonomous systems}, 59(5):265--273, 2011.

\bibitem{zhang2013svm}
Jixian Zhang, Xiangguo Lin, and Xiaogang Ning.
\newblock Svm-based classification of segmented airborne lidar point clouds in
  urban areas.
\newblock {\em Remote Sensing}, 5(8):3749--3775, 2013.

\bibitem{slaughter2008autonomous}
DC~Slaughter, DK~Giles, and D~Downey.
\newblock Autonomous robotic weed control systems: A review.
\newblock {\em Computers and electronics in agriculture}, 61(1):63--78, 2008.

\bibitem{dey2012classification}
Debadeepta Dey, Lily Mummert, and Rahul Sukthankar.
\newblock Classification of plant structures from uncalibrated image sequences.
\newblock In {\em Applications of Computer Vision (WACV), 2012 IEEE Workshop
  on}, pages 329--336. IEEE, 2012.

\bibitem{lehnert2017autonomous}
Christopher Lehnert, Andrew English, Christopher McCool, Adam~W Tow, and
  Tristan Perez.
\newblock Autonomous sweet pepper harvesting for protected cropping systems.
\newblock {\em IEEE Robotics and Automation Letters}, 2(2):872--879, 2017.

\bibitem{mccormick20163d}
Ryan~F Mccormick, Sandra~K Truong, and John~E Mullet.
\newblock 3d sorghum reconstructions from depth images identify qtl regulating
  shoot architecture.
\newblock {\em Plant Physiology}, pages pp--00948, 2016.

\bibitem{chaivivatrakul2014automatic}
Supawadee Chaivivatrakul, Lie Tang, Matthew~N Dailey, and Akash~D Nakarmi.
\newblock Automatic morphological trait characterization for corn plants via 3d
  holographic reconstruction.
\newblock {\em Computers and Electronics in Agriculture}, 109:109--123, 2014.

\bibitem{paulus2013surface}
Stefan Paulus, Jan Dupuis, Anne-Katrin Mahlein, and Heiner Kuhlmann.
\newblock Surface feature based classification of plant organs from 3d
  laserscanned point clouds for plant phenotyping.
\newblock {\em BMC bioinformatics}, 14(1):1, 2013.

\bibitem{chene2012use}
Yann Ch{\'e}n{\'e}, David Rousseau, Philippe Lucidarme, Jessica Bertheloot,
  Val{\'e}rie Caffier, Philippe Morel, {\'E}tienne Belin, and Fran{\c{c}}ois
  Chapeau-Blondeau.
\newblock On the use of depth camera for 3d phenotyping of entire plants.
\newblock {\em Computers and Electronics in Agriculture}, 82:122--127, 2012.

\end{thebibliography}
